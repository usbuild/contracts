const PrivateKeyProvider = require('truffle-privatekey-provider')
const privateKey = 'b8ba6ba0d4065f64834c13c7a872cd138032ad3df27a5551467bcb5528c8deff'
const provider = new PrivateKeyProvider(privateKey, 'https://rinkeby.infura.io/v3/b1a4ca90c9964c329c07874b37e05436')

module.exports = {
  compilers: {
    solc: {
      version: '^0.5.10',
      docker: false,
      settings: {
        optimizer: {
          enabled: false,
        },
      }
    }
  },
  networks: {
    development: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*',
    },
    rinkeby: {
      provider,
      network_id: 4,
    }
  },
}
