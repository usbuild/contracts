pragma solidity ^0.5.10;

contract User {
  string public name;
  string public email;
  address payable public owner;
  string[] public skills;
  string[] public sampleUrls;

  event PaymentReceived(uint weiValue, address sender);
  event UserUpdated(address payable owner, User user);

  constructor(address payable _owner) public {
    owner = _owner;
  }

  function () external payable {
    emit PaymentReceived(msg.value, msg.sender);
  }

  function withdraw() public {
    require(msg.sender == owner);
    owner.transfer(address(this).balance);
  }

  function setName(string memory _name) public {
    require(msg.sender == owner);
    name = _name;
    emit UserUpdated(owner, this);
  }

  function setEmail(string memory _email) public {
    require(msg.sender == owner);
    email = _email;
    emit UserUpdated(owner, this);
  }

  function setSkill(string memory _skill, uint index) public {
    require(msg.sender == owner);
    if (index >= skills.length) {
      skills.push(_skill);
      emit UserUpdated(owner, this);
      return;
    }
    skills[index] = _skill;
    emit UserUpdated(owner, this);
  }

  function setSampleUrl(string memory _url, uint index) public {
    require(msg.sender == owner);
    if (index >= sampleUrls.length) {
      sampleUrls.push(_url);
      emit UserUpdated(owner, this);
      return;
    }
    sampleUrls[index] = _url;
    emit UserUpdated(owner, this);
  }

  function skillCount() public view returns (uint) {
    return skills.length;
  }

  function sampleUrlCount() public view returns (uint) {
    return sampleUrls.length;
  }

  function isUser() public pure returns (bool) {
    return true;
  }
}
