pragma solidity ^0.5.10;

import "./User.sol";
import "./UsBuild.sol";

contract Team {
  string public name;
  User[] public users;
  address payable public owner;
  bytes32[] public inviteHashes;
  UsBuild public base;

  event TeamUpdated(address payable owner, Team team);
  event PaymentReceived(uint weiValue, address sender);

  constructor(address payable _owner, User first, UsBuild _base, string memory _name) public {
    owner = _owner;
    require(first.isUser());
    users.push(first);
    base = _base;
    name = _name;
  }

  function () external payable {
    emit PaymentReceived(msg.value, msg.sender);
  }

  function settle() public {
    if (users.length == 0) return;
    uint balance = address(this).balance;
    if (balance == 0) return;
    uint weiPerUser = balance / users.length;
    if (weiPerUser == 0) return;
    for (uint x = 0; x < users.length; x++) {
      address(users[x]).transfer(weiPerUser);
    }
  }

  function setName(string memory _name) public {
    require(msg.sender == owner);
    name = _name;
    emit TeamUpdated(owner, this);
  }

  function addUser(User user) public {
    require(msg.sender == owner);
    require(user.isUser());
    require(!userIsMember(user));
    settle();
    users.push(user);
    emit TeamUpdated(owner, this);
  }

  function removeUser(uint index) public {
    require(msg.sender == owner);
    require(index < users.length);
    settle();
    for (uint x = index; x < users.length - 1; x++) {
      users[x] = users[x + 1];
    }
    users.length--;
    emit TeamUpdated(owner, this);
  }

  function addInviteHash(bytes32 _hash) public {
    require(msg.sender == owner);
    inviteHashes.push(_hash);
    emit TeamUpdated(owner, this);
  }

  function isInviteHashValid(bytes32 _hash) public view returns (bool) {
    for (uint x = 0; x < inviteHashes.length; x++) {
      if (inviteHashes[x] == bytes32(0)) continue;
      if (inviteHashes[x] != _hash) continue;
      return true;
    }
    return false;
  }

  function joinTeam(bytes memory input) public {
    if (!base.userExists(msg.sender)) {
      base.createUser(msg.sender);
    }
    User user = base.userAccounts(msg.sender);
    bytes32 _hash = keccak256(input);
    for (uint x = 0; x < inviteHashes.length; x++) {
      if (inviteHashes[x] == bytes32(0)) continue;
      if (inviteHashes[x] != _hash) continue;
      inviteHashes[x] = bytes32(0);
      require(!userIsMember(user));
      settle();
      users.push(user);
      emit TeamUpdated(owner, this);
      return;
    }
    require(false);
  }

  function userIsMember(User user) public view returns (bool) {
    for (uint x = 0; x < users.length; x++) {
      if (address(users[x]) == address(user)) return true;
    }
    return false;
  }

  function userCount() public view returns (uint) {
    return users.length;
  }

  function inviteHashCount() public view returns (uint) {
    return inviteHashes.length;
  }

  function isTeam() public pure returns (bool) {
    return true;
  }
}
