pragma solidity ^0.5.10;

import "./User.sol";
import "./Team.sol";

contract UsBuild {

  mapping (address => User) public userAccounts;
  Team[] public teams;

  event UserCreated(address owner, User user);
  event TeamCreated(address owner, Team team);

  function createTeam(address payable owner, string memory _name) public returns (address) {
    /// Auto create an account
    if (!userExists(owner)) createUser(owner);
    require(userExists(owner));
    Team team = new Team(owner, userAccounts[owner], this, _name);
    teams.push(team);
    emit TeamCreated(owner, team);
    return address(team);
  }

  function createUser(address payable owner) public returns (address) {
    require(!userExists(owner));
    User user = new User(owner);
    userAccounts[owner] = user;
    emit UserCreated(owner, user);
    return address(user);
  }

  function userExists(address payable owner) public view returns (bool) {
    return address(userAccounts[owner]) != address(0x0);
  }

  function teamCount() public view returns (uint) {
    return teams.length;
  }

}
